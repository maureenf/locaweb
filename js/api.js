var exp ="";
var educ ="";
var prof = "";
$( document ).ready(function() {
    fetch('http://www.mocky.io/v2/587935ac260000420e1c3644').then(function(response) {
        return response.json();
    }).then(function(data) {
        $(".bg-load").fadeOut("slow");
        // Picture
        $("#profile-img").attr("src", data.thumb);
        $("#name").html(data.name);
        $("#profession").html(data.profession);
        // Profile
        $("#profile-title").html(data.profile.title);
        $("#profile-description").html(data.profile.description);
        // Objective
        $("#objective-title").html(data.objective.title);
        $("#objective-description").html(data.objective.description);

        // Personal Skill
        $("#skills-title").html(data.personalSkill.title);
        $.each(data.personalSkill, function(key, item) {
            $("#skills-"+ key).css("width", item +"%");
        });
        // Contact me
        $.each(data.contactMe, function(key, item) {
            $("#contactme-"+ key).html(item);
        });
        // Education
        $("#education-title").html(data.education.title);
        $.each(data.education.university, function(key, item) {
            educ += "<div class=\"row\">" +
                            "<div class=\"col-md-2\">"+
                                "<h5 class=\"text-center line-space\">"+item.month+"</h5>"+
                                "<h5 class=\"text-center line-space\">"+item.year+"</h5>"+
                            "</div>"+
                            "<div class=\"col-md-6\">"+
                              "<h5 ><strong>"+item.courseName+"</strong></h5>"+
                              "<h6 ><strong>"+item.universityName+", "+item.universityCity+"</strong></h6>"+
                              "<p class=\"text\">"+item.description+"</p>"+
                            "</div>"+
                        "</div>";
        });
        $("#university").html(educ);

        // Experience
        $("#experience-title").html(data.experience.title);
        $.each(data.experience.company, function(key, item) {
            exp += "<div class=\"row\">" +
                            "<div class=\"col-md-2\">"+
                                "<h5 class=\"text-center line-space\">"+item.month+"</h5>"+
                                "<h5 class=\"text-center line-space\">"+item.year+"</h5>"+
                            "</div>"+
                            "<div class=\"col-md-6\">"+
                              "<h5 class=\"text-uppercase\"><strong>"+item.companyName+"</strong></h5>"+
                              "<h6><strong>"+item.roleName+", "+item.companyCity+"</strong></h6>"+
                              "<p class=\"text\">"+item.description+"</p>"+
                            "</div>"+
                        "</div>";
        });
        $("#experience").html(exp);

        // Professional Skill
        $("#professional-skills-title").html(data.professionalSkill.title);
        $.each(data.professionalSkill.tools, function(key, item) {
            prof += "<div class=\"row fix-left\">" +
                "<div class=\"col-md-4\">" +
                    "<p class=\"skill-title text-uppercase\"><strong>"+ item.toolName+"</strong></p>" +
                "</div>" +
                "<div class=\"col-md-4\">" +
                    "<div class=\"progress\">" +
                        "<div class=\"progress-bar grey-bar\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:"+item.toolsSkill+"%\">" +
                        "</div>" +
                    "</div>" +
                "</div>" +
            "</div>";
        });
        $("#professional-skills").html(prof);

        // Award
        $("#award-title").html(data.award.title);
        $("#award-month").html(data.award.month);
        $("#award-year").html(data.award.year);
        $("#award-name").html(data.award.awardName);
        $("#award-project").html(data.award.projectName);
        $("#award-city").html(data.award.city);
        $("#award-description").html(data.award.description);

    }).catch(function(err) {
        console.log(err);
    });

});
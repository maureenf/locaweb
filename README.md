## Locaweb's pratical test
This project has the purpose to show you my code.
### Getting Started
Clone the repository.
You'll need to install [node](http://nodejs.org), [npm](http://nodejs.org) and [git](https://git-scm.com/) to run this project properly.
After that, you'll need to install Bower
``` $ npm install -g bower ```
Install all dependencies
``` $ bower install ```
Now, you'are ready to run the application.

### Built with
- Windows 10
- Gulp 3.9.1,
- JQuery 3.2.1,
- Bootstrap 3.3.7,
- Font Awesome 4.7.0,
- Fetch 2.0.3.

### Authors
Maureen Fernandes
<maureen_311@hotmail.com>

### Questions
* Explique o que é "hoisting".
É o comportamento de declarar variáveis no ínicio do escopo.
* O que é event bubbling.
O evento é chamado de "bubbling", porque os eventos "borbulham" do elemento interno (filho) para o elemento pai.
* Qual a diferença entre atributo e propriedade?
Atributo é uma informação default do elemento enquanto a propriedade são informações adicionadas e removidas em tempo de execução
* Qual a diferença entre == e ===?
'===' significa "igualdade estrita", ou seja, somente retorna true se os operandos forem do mesmo tipo e valor. Já o '==' precisa ser apenas do mesmo valor.